n1 = int(input('Digite um número: '))
n2 = int(input('Digite outro: '))

s = n1 + n2
m = n1 * n2
d = n1 / n2
di = n1 // n2
e = n1 ** n2

print('A soma é {0}, o produto é {1}, a divisão é {2:.3f}.'.format(s, m, d))
print('A divisão inteira é {0} e a potência é {1}.'.format(di, e))