import math

angulo = float(input('Digite o ângulo: '))

sen = math.sin(angulo)
cos = math.cos(angulo)
tan = math.tan(angulo)

print('O ângulo {0} tem:\nSeno: {1}\nCosseno: {2}\nTangente: {3}'.format(angulo, sen, cos,tan))