from math import trunc

number = float(input('Digite um número: '))
number_int = trunc(number)

print('O número {0} tem a parte inteira {1}.'.format(number, number_int))