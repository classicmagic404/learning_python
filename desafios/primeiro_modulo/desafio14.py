grausC = float(input('Informe a temperatura em °C: '))

grausF = grausC * 9 / 5 + 32

print('A temperatura {0}°C corresponda a {1:.1f}°F!'.format(grausC, grausF))