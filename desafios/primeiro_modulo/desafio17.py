co = float(input('Digite o cateto oposto: '))
ca = float(input('Digite o cateto adjacente: '))

hip = ((co ** 2) + (ca ** 2)) ** 0.5

print('O valor da hipotenusa é: {:.2f}'.format(hip))