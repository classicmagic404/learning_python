number = int(input('Digite um número: '))
dobro = number * 2
triplo = number * 3
raizquadrada = number ** (1/2)

print('Dobro: {}'.format(dobro))
print('Triplo: {}'.format(triplo))
print('Raiz quadrada: {}'.format(raizquadrada))