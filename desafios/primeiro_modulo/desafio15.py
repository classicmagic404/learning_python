km = float(input('Informe quantos km o carro rodou: '))
dias = int(input('E por quantos dias? '))

valor_pagar = (dias * 60) + (km * 0.15)

print('O valor que você terá que pagar é R${:.2f}.'.format(valor_pagar))